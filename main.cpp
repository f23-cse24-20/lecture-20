#include <iostream>
using namespace std;

void printArray(float arr[], int size) {
    for (int i = 0; i < size; i++) {
        cout << arr[i] << endl;
    }
}

// maxValue = 84.4
// maxPosition = 6

int findMaxValueIndex(float arr[], int size) {
    float maxValue = arr[0];
    int maxPosition = 0;

    for (int i = 0; i < size; i++) {
        if (arr[i] > maxValue) {
            maxValue = arr[i];
            maxPosition = i;
        }
    }

    return maxPosition;
}

int findMinValueIndex(float arr[], int size) {
    float minValue = arr[0];
    int minPosition = 0;

    for (int i = 0; i < size; i++) {
        if (arr[i] < minValue) {
            minValue = arr[i];
            minPosition = i;
        }
    }

    return minPosition;
}

void findHottestDay(float temps[], int size, string dayOfWeek[]) {
    int hottestTempIndex = findMaxValueIndex(temps, size);
    cout << "The hottest day was " << dayOfWeek[hottestTempIndex % 7] << " with a temperature of " << temps[hottestTempIndex] << endl;
}

void findColdestDay(float temps[], int size, string dayOfWeek[]) {
    int coldestTempIndex = findMinValueIndex(temps, size);
    cout << "The coldest day was " << dayOfWeek[coldestTempIndex % 7] << " with a temperature of " << temps[coldestTempIndex] << endl;
}

int main() {
    // Data we want to represent: average daily temperatures
    float week[7] = {78.9, 74.8, 71.7, 78.8, 72.2, 75.1, 84.4};
    float fourWeeks[28] = {72.7, 79.1, 81.4, 74.4, 83.2, 80.3, 75.1, 72.4, 72.9, 81.1, 83.1, 75.2, 85.9, 75.7, 77.1, 84.8, 83.2, 75.7, 80.7, 82.9, 74.3, 78.5, 76.5, 74.6, 170.2, 70.1, 74.7, 40.4};
    string dayOfWeek[7] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

    // Questions we want to answer:
    // What the hottest day of the week?
    // What the coldest day of the week?

    // The hottest day was Saturday with a temperature of 84.4.
    // The coldest day was Tuesday with a temperature of 71.7.

    // printArray(week, 7);

    findHottestDay(week, 7, dayOfWeek);
    findColdestDay(week, 7, dayOfWeek);

    cout << endl;

    findHottestDay(fourWeeks, 28, dayOfWeek);
    findColdestDay(fourWeeks, 28, dayOfWeek);

    return 0;
}

